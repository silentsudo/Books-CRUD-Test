/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ashish29agre.bookstest;

import in.ashish29agre.bookstest.client.Book;
import in.ashish29agre.bookstest.client.BookRestClient;
import java.time.LocalTime;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ashish
 */
public class BooksTest {
    private static final Logger  log = Logger.getLogger(BooksTest.class.getSimpleName());
    private final BookRestClient restClient;
    public BooksTest() {
        restClient = new BookRestClient();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        restClient.close();
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void createBook() {
         LocalTime localTime = LocalTime.now();
         Book book = new Book();
         book.setName("Java: The Complete Reference, Time:" + localTime);
         book.setAuthor("Herbert");
         book.setPublication("Publication");
         
         restClient.create_JSON(book);
         
         log.info("Book created");
     }
     
     @Test
     public void getBooksCount() {
         String count = restClient.countREST();
         log.info("Books count is:" + count);
     }
}
